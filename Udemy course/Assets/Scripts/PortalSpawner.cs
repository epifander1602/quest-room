using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalSpawner : MonoBehaviour
{
    [SerializeField] private LightChanger _lightChanger;
    [SerializeField] private Transform _portalParent;
    [SerializeField] private GameObject _portalPrefab;
    [SerializeField] private float _portalDistance = 2f;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameObject portalGo = SpawmPortal(); 
            ChangeLightAndPortalToRandomColor(portalGo);
        }
    }

    private GameObject SpawmPortal()
    {
        GameObject portalGo = Instantiate(_portalPrefab, _portalParent);
        portalGo.transform.localPosition = -Vector3.forward * _portalParent.childCount * _portalDistance + Vector3.up * 1f;
        return portalGo;

    }

    private void ChangeLightAndPortalToRandomColor(GameObject portalGo)
    {
        Color randomColor = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        //_lightChanger.ChangeLightColor(randomColor);
        portalGo.GetComponentInChildren<Portal>().SetLightColor(randomColor);
        List<MeshRenderer> _meshRenderers = new List<MeshRenderer>(portalGo.GetComponentsInChildren<MeshRenderer>());
        foreach (MeshRenderer meshRenderer in _meshRenderers)
        {
            if (meshRenderer.GetComponent<LightSwither>() != null)
            {
                continue;
            }
            meshRenderer.material.SetColor("_EmissionColor", randomColor);
        }
    }
}
