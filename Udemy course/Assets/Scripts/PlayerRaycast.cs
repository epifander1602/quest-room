using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRaycast : MonoBehaviour
{
    [SerializeField] private Camera _mainCamera;
    [SerializeField] private float _raycastDistance = 2f;
    [SerializeField] private LayerMask _raycastLayerMask;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            RaycastHit hit;
            if (Physics.Raycast(_mainCamera.transform.position, _mainCamera.transform.forward, out hit, _raycastDistance, _raycastLayerMask))
            {
                if (hit.collider.TryGetComponent(out LightSwither lightSwither));
                {
                    lightSwither.TurnOnLight();
                }
            }
            else
            {
                Debug.Log("We hit NOTHING");
            }
        }
        Debug.DrawLine(_mainCamera.transform.position, _mainCamera.transform.position + _mainCamera.transform.forward * _raycastDistance, Color.white);
    }
}
