using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForLoop : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            for (int i = 2; i < 10; i += 2)
            {
                if (i == 6)
                {
                    continue;
                }

                Debug.Log(i);
            }

            Debug.Log("For loop has been finished");
             
        }
    }
}
