using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSController : MonoBehaviour
{
    [SerializeField] private Rigidbody rig;
    [SerializeField] private float speed = 1;
    [SerializeField] private Transform cameraTransfore;
    [SerializeField] private float mouseSensitivity = 2;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        Vector3 cameraForwardDir = cameraTransfore.forward;
        cameraForwardDir.y = 0;
        Vector3 cameraRightDir = cameraTransfore.right;
        cameraRightDir.y = 0;

        Vector3 movementDir = cameraForwardDir.normalized * vertical + cameraRightDir.normalized * horizontal;
        movementDir = Vector3.ClampMagnitude(movementDir, 1) * speed;
        movementDir.y = rig.velocity.y;
        rig.velocity = movementDir;

        rig.angularVelocity = Vector3.zero;

        //if (cameraTransfore.rotation.eulerAngles.x - mouseY > 280 || cameraTransfore.rotation.eulerAngles.x - mouseY < 80)

        float newAngelX = cameraTransfore.rotation.eulerAngles.x - mouseY * mouseSensitivity;
        if (newAngelX > 180)
        {
            newAngelX = newAngelX - 360;
        }
        newAngelX = Mathf.Clamp(newAngelX, -80, 80);
        cameraTransfore.rotation = Quaternion.Euler(newAngelX, cameraTransfore.rotation.eulerAngles.y, cameraTransfore.rotation.eulerAngles.z);
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + mouseX * mouseSensitivity, transform.rotation.eulerAngles.z);

    }
}
