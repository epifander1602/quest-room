using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    [SerializeField] private int numberToSpawn = 1;
    [SerializeField] private GameObject objectToSpawn;
    [SerializeField]private int[,] array;

    private void Start()
    {
        array = new int[10,10];

        for (int i = 0; i < array.GetLength(0); i++)
        {
            for (int j = 0; j < array.GetLength(1); j++)
            {
                array[i, j] = i * array.GetLength(0) + j;
                //Debug.Log(array[i, j]);
            }
        }
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            int currentChileCount = transform.childCount;
            for (int i = currentChileCount; i < currentChileCount + numberToSpawn; i++)
            {
                GameObject spawnObject = Instantiate(objectToSpawn,transform);
                spawnObject.transform.localPosition = new Vector3Int(0, i, 0);
                spawnObject.transform.localRotation = Quaternion.Euler(new Vector3(0, 30 * i, 0));
            }
        }
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            int currentChileCount = transform.childCount;
            for (int i = currentChileCount - 1; i >= 0; i--)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
            //while (currentChileCount > 0)
            //{
            //    Destroy(transform.GetChild(currentChileCount - 1).gameObject);
            //    currentChileCount--;
            //}

        }
    }
}
