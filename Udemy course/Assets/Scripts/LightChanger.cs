using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using Color = UnityEngine.Color;

public class LightChanger : MonoBehaviour
{
    private enum InitialLight
    {
        Red, Blue, Purple
    }

    [SerializeField] private Color[] _initialColors;
    [SerializeField] private InitialLight initialLight = InitialLight.Purple;
    [SerializeField] private Light _light;
    [SerializeField] private Material _lightbulbMaterial;
    [SerializeField] private float _interpolationTime = 1f;
    // Start is called before the first frame update
    void Start()
    {
       Debug.Log(TurnOnInitialLight((int)initialLight));
    }

    public void ChangeLightColor(Color color)
    {
        _light.color = color;
    }

    public IEnumerator WaitToTurnOnLight(Color color)
    {
        //yield return new WaitForSeconds(2);
        float t = 0;
        Color startColor = _light.color;
        while (t < 1)
        {
            Color interpolatedColor = Color.Lerp(startColor, color, t);
            _lightbulbMaterial.SetColor("_EmissionColor", interpolatedColor);
            _light.color = interpolatedColor;
            t += Time.deltaTime / _interpolationTime;
            yield return null;
        }
    }

    public void TurnOnLight(Color color)
    {
        StartCoroutine(WaitToTurnOnLight(color));
    }

    private string TurnOnInitialLight(int lightIndex)
    {
        _light.color = _initialColors[lightIndex];
        _lightbulbMaterial.SetColor("_EmissionColor", _initialColors[lightIndex]);

        return "Light has been turned on";
    }

}
