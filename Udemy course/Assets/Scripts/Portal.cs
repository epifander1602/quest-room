using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    private Color _lightColor;
    [SerializeField] private LightChanger _lightChanger;


    private void Awake()
    {
        _lightChanger = FindObjectOfType<LightChanger>();
    }

    public void SetLightColor(Color color)
    {
        _lightColor = color;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<FPSController>() != null)
        {
            TurnOnLightThroughPortal();
        }
    }

    public void TurnOnLightThroughPortal()
    {
        _lightChanger.TurnOnLight(_lightColor);
    }
}
