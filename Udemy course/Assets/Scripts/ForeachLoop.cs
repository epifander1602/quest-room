using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForeachLoop : MonoBehaviour
{
    [SerializeField] private List<int> _integerList;
    [SerializeField] private int[] _integerArray = new int[] { 6, 7, 8, 9, 10 };

    private void Start()
    {
        _integerList = new List<int>(new int[] { 1, 2, 3, 4, });
        _integerList.Add(5);
        _integerList.AddRange(_integerArray);
        _integerList.Remove(10);
        _integerList.RemoveAt(2);

        foreach (int interger in _integerList)
        {
            Debug.Log(interger);
        }
        //for (int i = 0; i < _integerList.Count; i++)
        //{
        //    Debug.Log(_integerList[i]);
        //}
    }
    void Update()
    {
        
    }
}
